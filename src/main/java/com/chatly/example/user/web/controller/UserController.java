package com.chatly.example.user.web.controller;

import com.chatly.example.user.api.UserService;
import com.chatly.example.user.api.dto.UserDTO;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lihuazeng
 */
@RequestMapping("api/v1")
@RestController
public class UserController {
    /**
     * dubbo服务注入,使用@Autowired无效
     */
    @Reference
    private UserService userService;

    @PostMapping("user")
    public UserDTO create(@RequestBody UserDTO userDto){
        return userService.save(userDto);
    }
}
